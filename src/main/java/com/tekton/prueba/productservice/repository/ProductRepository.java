package com.tekton.prueba.productservice.repository;

import com.tekton.prueba.productservice.domain.Category;
import com.tekton.prueba.productservice.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {
    public List<Product> findByCategory(Category category);
}
