INSERT INTO tbl_categories (id, name) VALUES (1, 'shoes');
INSERT INTO tbl_categories (id, name) VALUES (2, 'books');
INSERT INTO tbl_categories (id, name) VALUES (3, 'electronics');

INSERT INTO tbl_products (id, name, description, stock,price,img,status,create_at,category_id)
VALUES (1, 'adidas Response Runner U','Tenis Adidas Hombre Running',5,160990.89,'tenis.png','CREATED','2018-09-05',1);

INSERT INTO tbl_products (id, name, description, stock,price,img,status,create_at,category_id)
VALUES (2, 'DESERT DERBY','DESERT DERBY - CALZADO CASUAL PARA HOMBRE',4,139900.5,'zapato01.jpg','CREATED','2018-09-05',1);


INSERT INTO tbl_products (id, name, description, stock,price,img,status,create_at,category_id)
VALUES (3, 'Breve historia del tiempo','Breve historia del tiempo (1988) examina la historia de las teorías científicas y las ideas que nos permiten entender cómo funciona el universo en la actualidad',12,45000.06,'HistoryTime.png','CREATED','2018-09-05',2);