package com.tekton.prueba.productservice.service;

import com.tekton.prueba.productservice.ProductServiceApplication;
import com.tekton.prueba.productservice.domain.Category;
import com.tekton.prueba.productservice.domain.Product;
import com.tekton.prueba.productservice.repository.CategoryRepository;
import com.tekton.prueba.productservice.repository.ProductRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ContextConfiguration(classes = ProductServiceApplication.class)
@SpringBootTest
@ActiveProfiles("test")
@DisplayName("ProductServiceTest :: Bateria de pruebas unitarias para el servicio")
class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @Mock
    private CategoryRepository categoryRepository;

    private ProductService productService;

    private Product computer;

    private Category laptops;

    @BeforeEach
    public void setup(){
        MockitoAnnotations.initMocks(this);

        productService = new ProductServiceImpl(productRepository,categoryRepository);
        computer = Product.builder()
                .id(1l)
                .name("computer")
                .description("Dell Latitud 3420")
                .stock(Double.parseDouble("10"))
                .price(Double.parseDouble("1240.99"))
                .category(Category.builder().id(1L).name("Laptops").build())
                .img("Latitude.png")
                .build();
        laptops = Category.builder()
                .id(1L)
                .name("Laptops")
                .build();
        List<Product> products = new ArrayList<>();
        products.add(computer);
        List<Category> categories = new ArrayList<>();
        categories.add(laptops);
        when(this.productRepository.findById(1L)).thenReturn(Optional.of(computer));
        when(this.productRepository.findById(2L)).thenReturn(Optional.empty());
        when(this.productRepository.save(computer)).thenReturn(computer);
        when(this.productRepository.save(Mockito.any(Product.class))).thenReturn(computer);
        when(this.productRepository.findAll()).thenReturn(products);
        when(this.productRepository.findByCategory(laptops)).thenReturn(products);
        when(this.categoryRepository.findAll()).thenReturn(categories);
        when(this.categoryRepository.findById(1L)).thenReturn(Optional.of(laptops));
        when(this.categoryRepository.findById(2L)).thenReturn(Optional.empty());
    }

    @Test
    @DisplayName("Test Service:: Get All Products succesfully")
    void listAllProduct() {
        List<Product> result = productService.listAllProduct();
        Assertions.assertThat(result.contains(computer));
    }

    @Test
    @DisplayName("Test Service:: Get Product succesfully")
    void whenValidGetID_ThenReturnProduct() {
        Product found = productService.getProduct(1L);
        Assertions.assertThat(found.getName()).isEqualTo("computer");
    }

    @Test
    @DisplayName("Test Service:: Get Product Not Exist")
    void whenInvalidGetID_ThenReturnNull()  {
        assertEquals(null, this.productService.getProduct(2L));
    }

    @Test
    @DisplayName("Test Service:: Save Product succesfully")
    void createProduct() {

        Product product = this.productService.createProduct(computer);
        System.out.println(product.getId());
        assertNotNull(String.valueOf(product.getId()));
        assertEquals(1, product.getId());
    }

    @Test
    @DisplayName("Test Service:: Save Product Exception")
    void whenCreateProductThrows() {
        when(this.productRepository.save(Mockito.any(Product.class))).thenReturn(null);
        assertEquals(null, this.productService.createProduct(computer));
    }

    @Test
    @DisplayName("Test Service:: Update Product succesfully")
    void updateProduct() {
        Product productUpdate = computer;
        productUpdate.setStatus("UPDATE");
        Product product = this.productService.updateProduct(productUpdate);
        assertEquals("UPDATE", product.getStatus());
    }

    @Test
    @DisplayName("Test Service:: Update Product NotExisty")
    void whenUpdateProductNotExist() {
        Product productUpdate = computer;
        productUpdate.setId(2L);
        productUpdate.setStatus("UPDATE");
        assertEquals(null, this.productService.updateProduct(productUpdate));
    }

    @Test
    @DisplayName("Test Service:: Delete Product succesfully")
    void deleteProduct() {
        Product productDelete = this.productService.deleteProduct(1L);
        assertEquals("DELETED", productDelete.getStatus());
    }
    @Test
    @DisplayName("Test Service:: Delete Product NotExisty")
    void whenDeleteProductNotExist() {
        Product productDelete = this.productService.deleteProduct(2L);
        assertEquals(null, productDelete);
    }

    @Test
    @DisplayName("Test Service:: Find by Category succesfully")
    void findByCategory() {
        List<Product> result = productService.findByCategory(laptops);
        Assertions.assertThat(result.contains(computer));
    }

    @Test
    @DisplayName("Test Service:: Find by Category NotExisty")
    void whenFindByCategoryNotExist() {
        List<Product> productsCategory = new ArrayList<>();
        when(this.productRepository.findByCategory(Mockito.any(Category.class))).thenReturn(productsCategory);
        List<Product> result = productService.findByCategory(new Category());
        Assertions.assertThat(result.isEmpty());
    }

    @Test
    @DisplayName("Test Service:: Update Stock succesfully")
    void whenValidUpdateStock_ThenReturnNewStock() {
        Product newStock = productService.updateStock(1L,Double.parseDouble("8"));
        Assertions.assertThat(newStock.getStock()).isEqualTo(18);
    }

    @Test
    @DisplayName("Test Service:: Create Category succesfully")
    void createCategory() {
        Category category = this.productService.createCategory(laptops);
        System.out.println(laptops.getId());
        assertNotNull(String.valueOf(laptops.getId()));
        assertEquals(1, laptops.getId());
    }

    @Test
    @DisplayName("Test Service:: Get All Categorys succesfully")
    void listAllCategory() {
        List<Category> result = productService.listAllCategory();
        Assertions.assertThat(result.contains(laptops));
    }

    @Test
    @DisplayName("Test Service:: Get Category succesfully")
    void getCategory() {
        Category found = productService.getCategory(1L);
        Assertions.assertThat(found.getName()).isEqualTo("Laptops");
    }
    @Test
    @DisplayName("Test Service:: Get Category Not Exist")
    void whenIValidCategoryID_ThenReturnNull()  {
        assertEquals(null, this.productService.getCategory(2L));
    }
}