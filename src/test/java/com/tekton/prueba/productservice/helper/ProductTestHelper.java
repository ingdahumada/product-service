package com.tekton.prueba.productservice.helper;

import com.tekton.prueba.productservice.domain.Category;
import com.tekton.prueba.productservice.domain.Product;

public class ProductTestHelper {

    /*public static PaymentControl createPaymentControl(){
        final PaymentControl paymentControl = new PaymentControl();
        paymentControl.setPaymentControlId("70a2babe-56a5-41a3-a330-cd9f859398be");
        paymentControl.setPolicyId("50004PD01320115648756300004100000062");
        paymentControl.setPolicyValidity(2);
        paymentControl.setRecurringStartDate("2022-05-19T14:15:22Z");
        paymentControl.setRecurringFinishDate("");
        paymentControl.setState("SUCCESS");
        final RecurringState recurringState = new RecurringState();
        recurringState.setName("SEND_TO_CORE");
        recurringState.setStartDate("2022-05-19T14:15:22Z");
        recurringState.setEndDate("2022-05-19T14:15:22Z");
        List<RecurringState> recurringStateList = new ArrayList<>();
        recurringStateList.add(recurringState);
        paymentControl.setSteps(recurringStateList);
        return paymentControl;
    }

    public static PaymentControlRequest createRequest(){
        final PaymentControlRequest paymentControlRequest = new PaymentControlRequest();
        paymentControlRequest.setPolicyId("50004PD01320115648756300004100000062");
        paymentControlRequest.setPolicyValidity(2);
        paymentControlRequest.setRecurringStartDate("2022-05-19T14:15:22Z");
        paymentControlRequest.setRecurringFinishDate("");
        paymentControlRequest.setState("SUCCESS");
        final RecurringState recurringState = new RecurringState();
        recurringState.setName("SEND_TO_CORE");
        recurringState.setStartDate("2022-05-19T14:15:22Z");
        recurringState.setEndDate("2022-05-19T14:15:22Z");
        List<RecurringState> recurringStateList = new ArrayList<>();
        recurringStateList.add(recurringState);
        paymentControlRequest.setSteps(recurringStateList);
        return paymentControlRequest;
    }*/

    public static Product queryRequest(){
        final Product productQueryRequest = Product.builder()
                .name("computer")
                .description("Dell Latitud 3420")
                .stock(Double.parseDouble("10"))
                .price(Double.parseDouble("1240.99"))
                .category(Category.builder().id(1L).name("Laptops").build())
                .img("Latitude.png")
                .build();
        return productQueryRequest;
    }

    public static Product queryResponse(){
        final Product ProductQueryResponse =
        Product.builder()
                .id(1l)
                .name("computer")
                .description("Dell Latitud 3420")
                .stock(Double.parseDouble("10"))
                .price(Double.parseDouble("1240.99"))
                .category(Category.builder().id(1L).name("Laptops").build())
                .img("Latitude.png")
                .build();
        return ProductQueryResponse;
    }
}
