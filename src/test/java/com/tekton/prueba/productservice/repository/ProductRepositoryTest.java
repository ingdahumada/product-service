package com.tekton.prueba.productservice.repository;

import com.tekton.prueba.productservice.domain.Category;
import com.tekton.prueba.productservice.domain.Product;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Date;
import java.util.List;

@DataJpaTest
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test
    @DisplayName("Test :: Find Product by Category")
    public void whenFindByCategory_thenReturnListProduct(){

        Product product01 = Product.builder()
                .name("computer")
                .category(Category.builder().id(1L).build())
                .description("Dell Latitud 3420")
                .stock(Double.parseDouble("10"))
                .price(Double.parseDouble("1240.99"))
                .img("Latitude.png")
                .status("Created")
                .createAt(new Date()).build();
        productRepository.save(product01);

        List<Product> founds = productRepository.findByCategory(product01.getCategory());
        Assertions.assertThat(founds.size()).isEqualTo(3);
    }

}
