package com.tekton.prueba.productservice.controller;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
@DisplayName("HealthCheckControllerUTest :: Bateria de pruebas unitarias para la operacion de validacion del estado del servicio")
class HealthCheckControllerUTest {
    /**
     * @author dahumada
     *
     */

    @Spy
    private HealthCheckController underTest;

    @Test
    @DisplayName("Test :: Valida la que la operacion de salud del servicio este up")
    void itShouldServicesUp() {
        ResponseEntity<String> response = this.underTest.healthCheck();

        assertThat(response.getBody()).contains("up!");
    }
}