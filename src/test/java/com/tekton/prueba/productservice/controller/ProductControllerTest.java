package com.tekton.prueba.productservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.tekton.prueba.productservice.domain.Product;
import com.tekton.prueba.productservice.helper.ProductTestHelper;
import com.tekton.prueba.productservice.service.ProductService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.ModelAndView;

import javax.xml.transform.Result;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.http.RequestEntity.post;

//@import(requestvalidator.class)
@ActiveProfiles("test")
@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers=ProductController.class)
@DisplayName("PaymentsControlControllerTest :: Bateria de pruebas unitarias para el controlador del servicio PaymentControl")
class ProductControllerTest {

    @MockBean
    private ProductService productService;

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private ProductController underTest;

    @Mock
    private BindingResult mockBindingResult;

    private ResponseEntity<ProductController> responseEntity;

    @BeforeEach
    void setUp()  {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    @DisplayName("Test Controller:: Get Product succesfully return http 200")
    void getProduct() {
        when(this.productService.getProduct(1L)).thenReturn(buildQueryResponse());
        ResponseEntity response = this.underTest.getProduct(1L);
        assertThat(response.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    }

    @Test
    @DisplayName("Test Controller:: Get Product Not Exist return http 404")
    void getProductNotExis() {
        when(this.productService.getProduct(1L)).thenReturn(null);
        ResponseEntity response = this.underTest.getProduct(1L);
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.NOT_FOUND.value());
    }

    @Test
    @DisplayName("Test Controller:: Get Product Exception return http 500")
    void getProductThrows() {
        when(this.productService.getProduct(1L)).thenThrow(NullPointerException.class);
        ResponseEntity response = this.underTest.getProduct(1L);
        assertThat(response.getStatusCodeValue()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @Test
    @DisplayName("Test Controller:: Create Product succesfully return http 201")
    void createProduct() throws Exception  {
        when(mockBindingResult.hasErrors()).thenReturn(false);
        when(this.productService.createProduct(buildQueryRequest())).thenReturn(buildQueryResponse());
        Product product = buildQueryRequest();
        ResponseEntity response = this.underTest.createProduct(product, mockBindingResult);
        assertThat(response.getStatusCode().value()).isEqualTo(HttpStatus.CREATED.value());
    }

    @Test
    @DisplayName("Test Controller:: Update Product succesfully return http 200")
    void updateProduct() {
        Product updateProduct = buildQueryRequest();
        updateProduct.setId(1L);
        when(this.productService.updateProduct(buildQueryRequest())).thenReturn(buildQueryResponse());
        ResponseEntity response = this.underTest.updateProduct(1L, buildQueryRequest());
        assertThat(response.getStatusCode().value()).isEqualTo(HttpStatus.OK.value());
    }

    private Product buildQueryRequest() {
        Product data = ProductTestHelper.queryRequest();
        return data;
    }

    private Product buildQueryResponse(){
        Product data = ProductTestHelper.queryResponse();
        return data;
    }
}